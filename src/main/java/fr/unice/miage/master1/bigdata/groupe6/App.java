package fr.unice.miage.master1.bigdata.groupe6;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.GRAPHEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.Post;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.JSONEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Product;
import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.KEYVALUEEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models.Feedback;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.RELATIONALEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Vendor;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.XMLEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice;
import fr.unice.miage.master1.bigdata.groupe6.models.DataList;
import fr.unice.miage.master1.bigdata.groupe6.queries.Query1;
import fr.unice.miage.master1.bigdata.groupe6.queries.Query4;

@SuppressWarnings("unused")
public class App {
	
	public static DataList dataList;
	
    public static void main(String[] args) throws Exception {
    	
    	loadAllData();
    	doQueries();
	}
    
    public static void loadAllData() throws ParserConfigurationException, SAXException, IOException {
    	System.out.println("Start loading data");
    	dataList = loadData();
    	System.out.println("Loading data ended");
    }
    
    public static void  doQueries() {
    	System.out.println("Start queries");
    	System.out.println(query1("4145").toString());
    	System.out.println(query4().toString());
    	System.out.println("Queries ended");
    }
    
    public static DataList loadData() throws ParserConfigurationException, SAXException, IOException {
    	System.out.println("Load customers from file and convert to list objects");
    	List<Customer> customers = RELATIONALEngine.loadCustomersFromFile();
    	System.out.println("Load vendors from file and convert to list objects");
    	List<Vendor> vendors = RELATIONALEngine.loadVendorsFromFile();
    	System.out.println("Load orders from file and convert to list objects");
    	List<Order> orders = JSONEngine.loadOrdersFromFile();
    	System.out.println("Load products from file and convert to list objects");
    	List<Product> products = JSONEngine.loadProductsFromFileCSV();
    	System.out.println("Load invoices from file and convert to list objects");
    	List<Invoice> invoices = XMLEngine.loadDataFromFile();
    	System.out.println("Load posts from file and convert to list objects");
    	List<Post> posts = GRAPHEngine.loadPostFromFileCSV();
    	System.out.println("Load feedbacks from file and convert to list objects");
    	List<Feedback> feedbacks = KEYVALUEEngine.loadDataFromFileCSV();
    	
    	System.out.println("Load linksCustomerCustomer from file and convert to list objects");
    	List<LinkCustomerCustomer> linksCustomerCustomer = GRAPHEngine.loadLinkCustomerCustomer();
    	System.out.println("Load linksCustomerTag from file and convert to list objects");
    	List<LinkCustomerTag> linksCustomerTag = GRAPHEngine.loadLinkCustomerTag();
    	System.out.println("Load linksPostTag from file and convert to list objects");
    	List<LinkPostTag> linksPostTag = GRAPHEngine.loadLinkPostTag();
    	System.out.println("Load linksPostCustomer from file and convert to list objects");
    	List<LinkPostCustomer> linksPostCustomer = GRAPHEngine.loadLinkPostCustomerFromFileCSV();
    	
    	return new DataList(customers, feedbacks, vendors, orders, products, invoices, posts, linksCustomerCustomer, linksCustomerTag, linksPostTag, linksPostCustomer);
    
    }
    
    public static Query1 query1(String idCustomer) {
    	
    	//customer
    	Customer customer = RELATIONALEngine.getCustomerById(idCustomer, dataList.getCustomers());
    	//orders
    	List<Order> orders = JSONEngine.getOrdersByCustomer(idCustomer, dataList.getOrders());
    	//invoices
    	List<Invoice> invoices = XMLEngine.getInvoicesByCustomer(idCustomer, dataList.getInvoices());
    	//feedbacks
    	List<Feedback> feedbacks = KEYVALUEEngine.getFeedbacksByCustomer(idCustomer, dataList.getFeedbacks());
    	//posts
    	List<Post> posts = GRAPHEngine.getPostsByCustomer(idCustomer, dataList.getPosts(), dataList.getLinkPostCustomer());
    	//tag + used
    	String tagMostUsed = GRAPHEngine.getTagIdMostUsedByPostsCustomer(posts,dataList.getLinksPostTag());
    	//product + ordered
    	Product productMostOrdered = JSONEngine.getProductAsinMostOrderedByInvoicesCustomer(invoices, dataList.getProducts());
    	
    	return new Query1(customer, orders, invoices, feedbacks, posts, tagMostUsed, productMostOrdered);
    	
    }
    
    public static Query4 query4() {
    	
    	// 2 customers who spend more
    	List<Customer> top2Customers = JSONEngine.getTop2CustomersSpendMore(dataList.getOrders(),dataList.getCustomers());
    	// friends
    	List<Customer> communFriends = XMLEngine.getCommonCustomers
    			(top2Customers.get(0), top2Customers.get(1), dataList.getLinksCustomerCustomer(), dataList.getCustomers());
		
    	return new Query4(top2Customers.get(0), top2Customers.get(1), communFriends);
    	
    }
    
    
    
	
}
