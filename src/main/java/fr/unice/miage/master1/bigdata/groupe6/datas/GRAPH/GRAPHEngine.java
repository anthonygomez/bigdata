package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.Post;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.RELATIONALEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;

public class GRAPHEngine {
	
	public static void launchLessHbase() throws FileNotFoundException {
		
		displayAll(loadLinkPostTag());
			
	}

	@Deprecated
	public static List<Customer> loadFriendsFromFileCSV(List<Customer> customers) throws FileNotFoundException { 
		
			String path = GRAPHEngine.class.getResource("person_knows_person_0_0.csv").getPath();
			
		    Scanner scanner = new Scanner(new File(path));
		    
		    scanner.next(); //ignore header
		    
		    scanner.useDelimiter("\\n");
		    
		    while (scanner.hasNext())
		    {
		    	
		        String[] datas = scanner.next().split("\\|");
		        
		        Customer customer = RELATIONALEngine.getCustomerById(datas[0].replaceAll("[\r\n]+",""),customers);
		        
		        if(customer != null) {
		        	
		        	customer.addFriend(datas[1].replaceAll("[\r\n]+",""));
		        	
		        }
		        
		        
		    }
		    
		    scanner.close();
		    
		    return customers;
        
	}
	
	public static List<Post> loadPostFromFileCSV() throws FileNotFoundException { 
		
		List<Post> posts = new ArrayList<Post>();
		
		String path = GRAPHEngine.class.getResource("post_0_0.csv").getPath();
		
	    Scanner scanner = new Scanner(new File(path));
	    
	    scanner.next(); //ignore header
	    
	    scanner.useDelimiter("\\n");
	    
	    while (scanner.hasNext())
	    {
	    	
	       String[] datas = scanner.next().split("\\|");
	        
	       posts.add(new Post(datas[0].replaceAll("[\r\n]+",""), datas[2].replaceAll("[\r\n]+",""), datas[3].replaceAll("[\r\n]+",""), datas[4].replaceAll("[\r\n]+",""), datas[6].replaceAll("[\r\n]+",""), datas[7].replaceAll("[\r\n]+","")));
	        
	    }
	    
	    scanner.close();
	    
	    return posts;
    
}
	@Deprecated
	public static List<Post> loadPostCreatorsFromFileCSV(List<Post> posts) throws FileNotFoundException { 
		
		String path = GRAPHEngine.class.getResource("post_hasCreator_person_0_0.csv").getPath();
		
	    Scanner scanner = new Scanner(new File(path));
	    
	    scanner.next(); //ignore header
	    
	    scanner.useDelimiter("\\n");
	    
	    while (scanner.hasNext())
	    {
	    	
	        String[] datas = scanner.next().split("\\|");
	        
	        Post post = GRAPHEngine.getPostById(datas[0].replaceAll("[\r\n]+",""),posts);
	        
	        if(post != null) {
	        	
	        	post.setCreator(datas[1].replaceAll("[\r\n]+",""));
	        	
	        }
	        
	    }
	    
	    scanner.close();
	    
	    return posts;
    
	}
	
	public static List<LinkPostCustomer> loadLinkPostCustomerFromFileCSV() throws FileNotFoundException { 
		
			List<LinkPostCustomer> linksPostCustomer = new ArrayList<LinkPostCustomer>();
			
			String path = GRAPHEngine.class.getResource("post_hasCreator_person_0_0.csv").getPath();
			
		    Scanner scanner = new Scanner(new File(path));
		    
		    scanner.next(); //ignore header
		    
		    scanner.useDelimiter("\\n");
		    
		    while (scanner.hasNext())
		    
		    {
		    	
		        String[] datas = scanner.next().split("\\|");
		        
		        linksPostCustomer.add(new LinkPostCustomer(datas[0].replaceAll("[\r\n]+",""), datas[1].replaceAll("[\r\n]+","")));
		   
		    }
		    
		    scanner.close();
		    
		    return linksPostCustomer;
	    
	}

	public static List<LinkCustomerCustomer> loadLinkCustomerCustomer() throws FileNotFoundException { 
		
		List<LinkCustomerCustomer> linksCustomerCustomer = new ArrayList<LinkCustomerCustomer>();
		
		String path = GRAPHEngine.class.getResource("person_knows_person_0_0.csv").getPath();
		
	    Scanner scanner = new Scanner(new File(path));
	    
	    scanner.next(); //ignore header
	    
	    scanner.useDelimiter("\\n");
	    
	    while (scanner.hasNext())
	    {
	    	
	    	String[] datas = scanner.next().split("\\|");
	        
	    	linksCustomerCustomer.add(new LinkCustomerCustomer(datas[0].replaceAll("[\r\n]+",""), datas[1].replaceAll("[\r\n]+",""), datas[2].replaceAll("[\r\n]+","")));
	        	
	    }
	    
	    scanner.close();
	    
	    return linksCustomerCustomer;
	
	}

	public static List<LinkCustomerTag> loadLinkCustomerTag() throws FileNotFoundException { 
		
		List<LinkCustomerTag> linksCustomerTag = new ArrayList<LinkCustomerTag>();
		
		String path = GRAPHEngine.class.getResource("person_hasInterest_tag_0_0.csv").getPath();
		
	    Scanner scanner = new Scanner(new File(path));
	    
	    scanner.next(); //ignore header
	    
	    scanner.useDelimiter("\\n");
	    
	    while (scanner.hasNext())
	    {
	    	
	    	String[] datas = scanner.next().split("\\|");
	        
	    	linksCustomerTag.add(new LinkCustomerTag(datas[0].replaceAll("[\r\n]+",""), datas[1].replaceAll("[\r\n]+","")));
	        	
	    }
	    
	    scanner.close();
	    
	    return linksCustomerTag;
	
	}
	
	public static List<LinkPostTag> loadLinkPostTag() throws FileNotFoundException { 
			
			List<LinkPostTag> linksPostTag = new ArrayList<LinkPostTag>();
			
			String path = GRAPHEngine.class.getResource("post_hasTag_tag_0_0.csv").getPath();
			
		    Scanner scanner = new Scanner(new File(path));
		    
		    scanner.next(); //ignore header
		    
		    scanner.useDelimiter("\\n");
		    
		    while (scanner.hasNext())
		    {
		    	
		    	String[] datas = scanner.next().split("\\|");
		        
		    	linksPostTag.add(new LinkPostTag(datas[0].replaceAll("[\r\n]+",""), datas[1].replaceAll("[\r\n]+","")));
		        	
		    }
		    
		    scanner.close();
		    
		    return linksPostTag;
		
		}

	
	public static Post getPostById(String id,List<Post> posts) {
		
		return posts.stream()
				  .filter(post -> id.contentEquals(post.getId()))
				  .findFirst()
				  .orElse(null);
		
	}
	
	public static void displayAll(List<?> listLoaded) {
		for ( Object object : listLoaded) {
			System.out.println(object.toString());
		}
	}
	
	public static List<Post> getPostsByCustomer(String idCustomer, List<Post> posts, List<LinkPostCustomer> linksPostCustomer) {
		
		List<Post> postsByCustomer = new ArrayList<Post>();
		
		List<LinkPostCustomer> linksPostCustomerByCustomer = linksPostCustomer.stream()
				.filter(linkPostCustomer -> idCustomer.contentEquals(linkPostCustomer.getCustomerId()))
				.collect(Collectors.toList());
		
		for (LinkPostCustomer linkPostByCustomer : linksPostCustomerByCustomer) {
			
			Post post = getPostById(linkPostByCustomer.getPostId(), posts);
			
			if (post != null) {
				
				postsByCustomer.add(post);
				
			}
			
		}
		
		return postsByCustomer;
		
	}
	
	public static String getTagIdByPost(Post post, List<LinkPostTag> linksPostTag) {
			
		return linksPostTag.stream()
				  .filter(linkPostTag -> post.getId().contentEquals(linkPostTag.getPostId()))
				  .findFirst()
				  .orElse(null)
				  .getTagId();
	}
	
	public static LinkPostTag getLinkPostTagByPost(Post post, List<LinkPostTag> linksPostTag) {
		
		return linksPostTag.stream()
				  .filter(linkPostTag -> post.getId().contentEquals(linkPostTag.getPostId()))
				  .findFirst()
				  .orElse(null);
	}
	
public static LinkPostTag getLinkPostTagByPostId(String postId, List<LinkPostTag> linksPostTag) {
		
		return linksPostTag.stream()
				  .filter(linkPostTag -> postId.contentEquals(linkPostTag.getPostId()))
				  .findFirst()
				  .orElse(null);
	}
	
	public static String getTagIdMostUsedByPostsCustomer(List<Post> postsCustomer, List<LinkPostTag> linksPostTag) {
		
		String tagIdMostUsed = "";
		
		LinkPostTag tagMostUsed = postsCustomer.stream()
		        // map person to tag & filter null tag out 
		        .map(post -> getLinkPostTagByPostId(post.getId(), linksPostTag)).filter(Objects::nonNull)
		        // summarize tags
		        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
		        // fetch the max entry
		        .entrySet().stream().max(Map.Entry.comparingByValue())
		        // map to tag
		        .map(Map.Entry::getKey).orElse(null);
		
		if(tagMostUsed != null) {
			tagIdMostUsed = tagMostUsed.getTagId();
		}
			
		return tagIdMostUsed;
			
	}
	
}
