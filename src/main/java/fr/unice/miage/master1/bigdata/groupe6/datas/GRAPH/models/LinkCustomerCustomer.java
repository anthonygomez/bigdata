package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models;

public class LinkCustomerCustomer {
	
	private String customerId1;
	private String customerId2;
	private String creationDate;
	
	public LinkCustomerCustomer(String customerId1, String customerId2, String creationDate) {
		this.customerId1 = customerId1;
		this.customerId2 = customerId2;
		this.creationDate = creationDate;
	}

	public String getCustomerId1() {
		return customerId1;
	}

	public String getCustomerId2() {
		return customerId2;
	}

	public String getCreationDate() {
		return creationDate;
	}

	@Override
	public String toString() {
		return "LinkCustomerCustomer [customerId1=" + customerId1 + ", customerId2=" + customerId2 + ", creationDate="
				+ creationDate + "]";
	}

}
