package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models;

public class LinkCustomerTag {
	
	private String CustomerId;
	private String TagId;
	
	public LinkCustomerTag(String customerId, String tagId) {
		CustomerId = customerId;
		TagId = tagId;
	}

	public String getCustomerId() {
		return CustomerId;
	}

	public String getTagId() {
		return TagId;
	}

	@Override
	public String toString() {
		return "LinkCustomerTag [CustomerId=" + CustomerId + ", TagId=" + TagId + "]";
	}	

}
