package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models;

public class LinkPostCustomer {
	
	private String PostId;
	private String CustomerId;
	
	public LinkPostCustomer(String postId, String customerId) {
		PostId = postId;
		CustomerId = customerId;
	}

	public String getPostId() {
		return PostId;
	}

	public String getCustomerId() {
		return CustomerId;
	}

	@Override
	public String toString() {
		return "LinkPostCustomer [PostId=" + PostId + ", CustomerId=" + CustomerId + "]";
	}

}
