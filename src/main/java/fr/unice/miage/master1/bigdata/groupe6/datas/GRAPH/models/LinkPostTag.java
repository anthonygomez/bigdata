package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models;

public class LinkPostTag {
	
	private String PostId;
	private String TagId;
	
	public LinkPostTag(String postId, String tagId) {
		PostId = postId;
		TagId = tagId;
	}

	public String getPostId() {
		return PostId;
	}

	public String getTagId() {
		return TagId;
	}

	@Override
	public String toString() {
		return "LinkPostTag [PostId=" + PostId + ", TagId=" + TagId + "]";
	}

}
