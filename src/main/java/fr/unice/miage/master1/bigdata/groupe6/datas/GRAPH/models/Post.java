package fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models;

public class Post {
	private String id;
	private String createdate;
	private String location;
	private String browserUsed;
	private String content;
	private String length;
	
	private String creatorId;
	
	public Post(String id, String createdate, String location, String browserUsed, String content, String length) {
		this.id = id;
		this.createdate = createdate;
		this.location = location;
		this.browserUsed = browserUsed;
		this.content = content;
		this.length = length;
		this.creatorId = "";
	}

	public Post(String id, String createdate, String location, String browserUsed, String content, String length,
			String creatorId) {
		this.id = id;
		this.createdate = createdate;
		this.location = location;
		this.browserUsed = browserUsed;
		this.content = content;
		this.length = length;
		this.creatorId = creatorId;
	}


	public String getId() {
		return id;
	}

	public String getCreatedate() {
		return createdate;
	}

	public String getLocation() {
		return location;
	}

	public String getBrowserUsed() {
		return browserUsed;
	}

	public String getContent() {
		return content;
	}

	public String getLength() {
		return length;
	}

	public String getCreatorId() {
		return creatorId;
	}
	
	public void setCreator(String customerId) {
		this.creatorId = customerId;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", createdate=" + createdate + ", location=" + location + ", browserUsed="
				+ browserUsed + ", content=" + content + ", length=" + length + ", creatorId=" + creatorId + "]";
	}

}
