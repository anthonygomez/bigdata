package fr.unice.miage.master1.bigdata.groupe6.datas.JSON;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Brand;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Orderline;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Product;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice;
import fr.unice.miage.master1.bigdata.groupe6.hbase.HBaseEngine;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;

public class JSONEngine {
	
	public static void launch() throws Exception {
		
		HBaseEngine hbaseClient = new HBaseEngine();
		
		importDatasToHBase(hbaseClient);
		exportDatasFromHBase(hbaseClient);
		
	}
	
	public static void launchLessHbase() throws Exception {
		
		System.out.println("Start");
		
		List<Product> products = loadProductsFromFileCSV();
		displayAll(products);
		
		List<Order> orders = loadOrdersFromFile();
		displayAll(orders);
		
		
		System.out.println("End");
	}

	
	public static void importDatasToHBase(HBaseEngine hbaseClient) throws Exception {
		
		// Load JSON data into list of Order objects
		List<Order> orders = loadOrdersFromFile();
		// Display list of Order objects 
		//displayAllOrders(orders);
		System.out.println(getOrderByOrderId(orders, "122edb14-7915-47d0-ac20-10c1c8348bec").toString());
		
		// Convert list of Order object to list of Record object
		List<Record> recordsOrders = objectsToRecords(orders);
		
		// Store records into HBase
		storeRecordsToHBase(hbaseClient,recordsOrders);
		
	}
	
	public static void exportDatasFromHBase(HBaseEngine hbaseClient) throws Exception {
		
		// Load HBase data into list of Record objects
		List<Record> recordsOrders = loadDataFromHBase(hbaseClient);
		
		// Convert list of Record object to list of Order object
		List<Order> orders = recordsToObjects(recordsOrders);
		// Display list of Order objects 
		//displayAllOrders(orders);
		System.out.println(getOrderByOrderId(orders, "122edb14-7915-47d0-ac20-10c1c8348bec").toString());
		
	}
	
	public static List<Order> loadOrdersFromFile() throws JSONException, FileNotFoundException {
        
		String path = JSONEngine.class.getResource("Order.json").getPath();
		JSONArray ordersJSON = new JSONArray((new JSONTokener(new FileReader(path))));
		
		List<Order> orders = new ArrayList<Order>();
		
		for(Object orderJSON : ordersJSON) {
			
			if (orderJSON instanceof JSONObject) {
				
				String OrderDate =  ((JSONObject) orderJSON).getString("OrderDate");
				String OrderId = ((JSONObject) orderJSON).getString("OrderId");
				String PersonId = ((JSONObject) orderJSON).getString("PersonId");
				Double TotalPrice = ((JSONObject) orderJSON).getDouble("TotalPrice");
				
				JSONArray OrderlinesJSON = ((JSONObject) orderJSON).getJSONArray("Orderline");
				
				List<Orderline> orderlines = new ArrayList<Orderline>();
				
				for(Object OrderlineJSON : OrderlinesJSON) {
					
					if (OrderlineJSON instanceof JSONObject) {
						
						String asin =  ((JSONObject) OrderlineJSON).getString("asin");
						String brand = ((JSONObject) OrderlineJSON).getString("brand");
						Double price = ((JSONObject) OrderlineJSON).getDouble("price");
						String productId = ((JSONObject) OrderlineJSON).getString("productId");
						String title =  ((JSONObject) OrderlineJSON).getString("title");
						
						orderlines.add(new Orderline(productId, asin, title, price, brand));
						
					}
					
				}
				
				orders.add(new Order(OrderId,PersonId,OrderDate,TotalPrice,orderlines));
				
			}
			
		}
		
		return orders;
		
	}
	
	public static void displayAll(List<?> listLoaded) {
		for ( Object object : listLoaded) {
			System.out.println(object.toString());
		}
	}
	
	
	public static List<Record> objectsToRecords(List<Order> orders){
		
		List<Record> records = new ArrayList<Record>();
		for (Order order : orders) {
			records.add(order.toRecord());
		}
		
		return records;
		
	}
	
	public static void storeRecordsToHBase(HBaseEngine hbaseClient,List<Record> records) throws Exception {
		
		System.out.println("\nInitialize Hbase client ... \n");
		
		System.out.println("\nDeclare table structure into Hbase database ... \n");
		boolean tableIsCreated = hbaseClient.createTable(Order.generateTableStruct());
		
		if(tableIsCreated) {
			System.out.println("Put record objects into Hbase database ...\n");
			hbaseClient.createRecords(records);
		}
		else {
			System.err.println("\n[WARN] Datas in table already exist ... ignoring datas insertions\n");
		}
		
	}
	
	public static List<Record> loadDataFromHBase(HBaseEngine hbaseClient) {
		
		return hbaseClient.getRecords("Order");
	
	}
	
	public static List<Order> recordsToObjects(List<Record> records){
			
			List<Order> orders = new ArrayList<Order>();
		
			for (Record record : records) {
				orders.add(record.toOrder());
			}
			
			return orders;
			
		}
	
	public static Order getOrderByOrderId(List<Order> orders, String orderId) {
		
		Order orderReturn = null;
		
		for (Order order : orders) {
			
			if(order.getOrderId().contentEquals(orderId)) {
				orderReturn = order;
				return orderReturn;
			}
		}
		
		return orderReturn;
	}
	
	@Deprecated
	public static List<Product> loadProductsFromFileCSV2() throws FileNotFoundException {
        
		List<Product> productsWithoutBrands = loadProductsFromFileCSV();
		List<Brand> brands = loadDataBrandFromFileCSV();
		return buildProductList(productsWithoutBrands, brands);
        
}
	
	
	public static List<Product> loadProductsFromFileCSV() throws FileNotFoundException {
		
			String regex = "\\," + "(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)";
		
			List<Product> products = new ArrayList<Product>();
	        
			String path = JSONEngine.class.getResource("Product.csv").getPath();
			
	        Scanner scanner = new Scanner(new File(path));
	        
	        scanner.useDelimiter("\\n");
	        
	        while (scanner.hasNext())
	        {
	        	
	            String[] datas = scanner.next().split(regex);
	            
	            products.add(new Product(datas[0], datas[1], datas[2], datas[3]));
	            
	        }
	        
	        scanner.close();
	        
	        return products;
	        
	}
	
	
	public static List<Brand> loadDataBrandFromFileCSV() throws FileNotFoundException {
		
		List<Brand> brands = new ArrayList<Brand>();
        
		String path = JSONEngine.class.getResource("BrandByProduct.csv").getPath();
		
        Scanner scanner = new Scanner(new File(path));
        
        scanner.useDelimiter("\\n");
        
        while (scanner.hasNext())
        {
        	
            String[] datas = scanner.next().split("\\,");
            
            brands.add(new Brand(datas[0].replaceAll("[\r\n]+", ""), datas[1].replaceAll("[\r\n]+", "")));
            
        }
        
        scanner.close();
        
        return brands;
        
}
	
	public static Product getProductByASIN(String asin,List<Product> products) {
		
		return products.stream()
				  .filter(product -> asin.contentEquals(product.getAsin()))
				  .findFirst()
				  .orElse(null);
		
	}
	
	public static Brand getBrandByASIN(String asin,List<Brand> brands) {
			
			return brands.stream()
					  .filter(product -> asin.contentEquals(product.getAsin()))
					  .findFirst()
					  .orElse(null);
			
		}
	
	@Deprecated
	public static List<Product> buildProductList(List<Product> products, List<Brand> brands) {
		
		List<Product> productsBuilded = new ArrayList<Product>();
		
		for (Brand brand : brands) {
			
			Product productFinded = getProductByASIN(brand.getAsin(), products);
			
			if(productFinded != null) {
				
				productsBuilded.add(new Product(productFinded.getAsin(), productFinded.getTitle(), productFinded.getPrice(),
						productFinded.getImgUrl(), brand.getBrand()));
			
			}
			
		}
		
		return productsBuilded;
		
	}
	
	public static List<Order> getOrdersByCustomer(String idCustomer,List<Order> orders) {
		
		return orders.stream()
				  .filter(order -> idCustomer.contentEquals(order.getPersonId()))
				  .collect(Collectors.toList());
		
	}
	
	public static Product getProductAsinMostOrderedByInvoicesCustomer(List<Invoice> invoicesCustomer, List<Product> products) {
		
		List<Orderline> orderlines = new ArrayList<Orderline>();
		
		for (Invoice invoice : invoicesCustomer) {
			for(Orderline orderline : invoice.getOrderlines()) {
				
				orderlines.add(orderline);
				
			}
		}
		
		String asinProductMostOrdered = orderlines.stream()
		        .map(orderline -> getProductByASIN(orderline.getAsin(),products).getAsin()).filter(Objects::nonNull)
		        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
		        .entrySet().stream().max(Map.Entry.comparingByValue())
		        .map(Map.Entry::getKey).orElse(null);
		
		return getProductByASIN(asinProductMostOrdered, products);
	}
	
	public static List<Customer> getTop2CustomersSpendMore(List<Order> orders, List<Customer> customers){
		
		List<Customer> top2CustomersSpendMore = new ArrayList<Customer>();
		
		Customer top1Customer = null;
		double top1totalSpent = 0;
		
		Customer top2Customer = null;
		double top2totalSpent = 0;
		
		
		for (Customer customer : customers) {
			
			double totalSpent = 0;
			
			List<Order> ordersCustomer = getOrdersByCustomer(customer.getId(), orders);
			
			for (Order order : ordersCustomer) {
				
				List<Orderline> orderlinesCustomer = order.getOrderlines();
				
				for (Orderline orderline : orderlinesCustomer) {
					
					totalSpent += orderline.getPrice();
					
				}
				
			}
			
			if(totalSpent > top1totalSpent) {
				top1totalSpent = totalSpent;
				top1Customer = customer;
			}
			
			else if(totalSpent > top2totalSpent) {
				top2totalSpent = totalSpent;
				top2Customer = customer;
			}
			
		}
		
		top2CustomersSpendMore.add(top1Customer);
		top2CustomersSpendMore.add(top2Customer);
		
		return top2CustomersSpendMore;
		
	}
	
}
