package fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models;

public class Brand {
	
	private String brand;
	private String asin;
	
	public Brand(String brand, String asin) {
		this.brand = brand;
		this.asin = asin;
	}

	public String getBrand() {
		return brand;
	}

	public String getAsin() {
		return asin;
	}

	@Override
	public String toString() {
		return "Brand [brand=" + brand + ", asin=" + asin + "]";
	}
	
}
