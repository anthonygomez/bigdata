package fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Data;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.TableStruct;

public class Order {
	
	private String OrderId;
	private String PersonId;
	private String OrderDate;
	private double TotalPrice;
	
	private List<Orderline> Orderlines;
	
	public Order(String orderId, String personId, String orderDate, Double totalPrice, List<Orderline> orderlines) {
		OrderId = orderId;
		PersonId = personId;
		OrderDate = orderDate;
		TotalPrice = totalPrice;
		Orderlines = orderlines;
	}

	public String getOrderId() {
		return OrderId;
	}

	public String getPersonId() {
		return PersonId;
	}

	public String getOrderDate() {
		return OrderDate;
	}

	public double getTotalPrice() {
		return TotalPrice;
	}

	public List<Orderline> getOrderlines() {
		return Orderlines;
	}
	
	
	@Override
	public String toString() {
		return "Order [OrderId=" + OrderId + ", PersonId=" + PersonId + ", OrderDate=" + OrderDate + ", TotalPrice="
				+ TotalPrice + ", Orderlines=" + Orderlines + "]";
	}

	public Record toRecord() {
		
		ArrayList<Data> datas = new ArrayList<Data>();
		
		datas.add(new Data("order", "date", this.OrderDate));
		datas.add(new Data ("order", "totalprice", String.valueOf(this.TotalPrice)));
		
		datas.add(new Data("person", "id", this.PersonId));
		
		for (Orderline orderline : this.Orderlines) {
			
			datas.add(new Data("orderline", "asin", orderline.getAsin()));
			datas.add(new Data("orderline", "brand", orderline.getBrand()));
			datas.add(new Data("orderline", "price", String.valueOf(orderline.getPrice())));
			datas.add(new Data("orderline", "productId", orderline.getProductId()));
			datas.add(new Data("orderline", "title", orderline.getTitle()));
			
		}
		
		return new Record("Order", OrderId, datas);
		
	}
	
	public static TableStruct generateTableStruct() {
			
			List<String> families = Arrays.asList("order","person","orderline");
			
			return new TableStruct("Order", families);
			
		}

}
