package fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models;

public class Orderline {
	
	private String productId;
	private String asin;
	private String title;
	private double price;
	private String brand;
	
	public Orderline(String productId, String asin, String title, double price, String brand) {
		this.productId = productId;
		this.asin = asin;
		this.title = title;
		this.price = price;
		this.brand = brand;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Orderline [productId=" + productId + ", asin=" + asin + ", title=" + title + ", price=" + price
				+ ", brand=" + brand + "]";
	}

	
}
