package fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models;

public class Product {
	
	private String asin;
	private String title;
	private String price;
	private String imgUrl;
	private String brand;
	
	public Product(String asin, String title, String price, String imgUrl, String brand) {
		this.asin = asin;
		this.title = title;
		this.price = price;
		this.imgUrl = imgUrl;
		this.brand = brand;
	}
	
	public Product(String asin, String title, String price, String imgUrl) {
		this.asin = asin;
		this.title = title;
		this.price = price;
		this.imgUrl = imgUrl;
		this.brand = "";
	}

	public String getAsin() {
		return asin;
	}

	public String getTitle() {
		return title;
	}

	public String getPrice() {
		return price;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Product [asin=" + asin + ", title=" + title + ", price=" + price + ", imgUrl=" + imgUrl + ", brand="
				+ brand + "]";
	}

}
