package fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models.Feedback;

public class KEYVALUEEngine {

	public static void launchLessHbase() throws Exception {
		
		System.out.println("Start");
		// Load JSON data into list of Order objects
		List<Feedback> feedbackIn = loadDataFromFileCSV();
		displayAllFeedback(feedbackIn);
		System.out.println("End");
	}
	
	public static void displayAllFeedback(List<Feedback> feedbacksLoaded) {
		for ( Feedback feedbackLoaded : feedbacksLoaded) {
			System.out.println(feedbackLoaded.toString());
		}
	}
	
	public static List<Feedback> loadDataFromFileCSV() throws FileNotFoundException {
		
		List<Feedback> feedback = new ArrayList<Feedback>();
        
		String path = KEYVALUEEngine.class.getResource("Feedback.csv").getPath();
		
        Scanner scanner = new Scanner(new File(path));
        
        scanner.useDelimiter("\\n");

        while (scanner.hasNext())
        { 
            String[] datas = scanner.next().split("\\|");
            
            feedback.add(new Feedback(datas[0], datas[1], datas[2]));
            
        }
        
        scanner.close();
        
        return feedback;
        
	}
	
	public static List<Feedback> getFeedbacksByCustomer(String id,List<Feedback> feedbacks) {
		
		return feedbacks.stream()
				  .filter(feedback -> id.contentEquals(feedback.getPersonId()))
				  .collect(Collectors.toList());
		
	}
	
}
