package fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models;

import java.util.ArrayList;

import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Data;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;

public class Feedback {

	private String asin;
	private String PersonId;
	private String feedback;
	
	public Feedback(String pAsin, String pPersonId, String pFeedback) {
		this.asin = pAsin;
		this.PersonId = pPersonId;
		this.feedback = pFeedback;
	}
	
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public String getPersonId() {
		return PersonId;
	}
	public void setPersonId(String personId) {
		PersonId = personId;
	}
	public String getAsin() {
		return asin;
	}
	public void setAsin(String asin) {
		this.asin = asin;
	}

	@Override
	public String toString() {
		return "Feedback [asin=" + asin + ", PersonId=" + PersonId + ", feedback=" + feedback + "]";
	}
	
	public Record toRecord() {
		
		ArrayList<Data> datas = new ArrayList<Data>();

		datas.add(new Data("person", "id", this.PersonId));
		datas.add(new Data("infos", "feedback", this.feedback));
		
		return new Record("Feedback", asin, datas);
		
	}
}
