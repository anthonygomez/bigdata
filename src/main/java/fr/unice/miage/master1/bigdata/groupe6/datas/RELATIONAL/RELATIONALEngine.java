package fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Vendor;

public class RELATIONALEngine {
	
	public static void launchLessHbase() throws FileNotFoundException {
		
		displayAll(loadCustomersFromFile());
		
	}
	
	public static List<Customer> loadCustomersFromFile() throws FileNotFoundException{
			
			List<Customer> customers = new ArrayList<Customer>();
		    
			String path = RELATIONALEngine.class.getResource("person_0_0.csv").getPath();
			
		    Scanner scanner = new Scanner(new File(path));
		    
		    scanner.next(); //ignore header
		    
		    scanner.useDelimiter("\\n");
		    
		    while (scanner.hasNext())
		    {
		    	
		        String[] datas = scanner.next().split("\\|");
		        
		        customers.add(new Customer(datas[0].replaceAll("[\r\n]+", ""), datas[1].replaceAll("[\r\n]+", ""),datas[2].replaceAll("[\r\n]+", ""),datas[3].replaceAll("[\r\n]+", ""),datas[4].replaceAll("[\r\n]+", ""),datas[5].replaceAll("[\r\n]+", ""),datas[6].replaceAll("[\r\n]+", ""),datas[7].replaceAll("[\r\n]+", ""),Integer.valueOf(datas[8].replaceAll("[\r\n]+", ""))));
		        
		    }
		    
		    scanner.close();
		    
		    return customers;
			
		}
	
	public static Customer getCustomerById(String id,List<Customer> customers) {
			
			return customers.stream()
					  .filter(customer -> id.contentEquals(customer.getId()))
					  .findFirst()
					  .orElse(null);
			
		}
	
	public static List<Vendor> loadVendorsFromFile() throws FileNotFoundException{
		
		List<Vendor> vendors = new ArrayList<Vendor>();
	    
		String path = RELATIONALEngine.class.getResource("Vendor.csv").getPath();
		
	    Scanner scanner = new Scanner(new File(path));
	    
	    scanner.next(); //ignore header
	    
	    scanner.useDelimiter("\\n");
	    
	    while (scanner.hasNext())
	    {
	    	
	        String[] datas = scanner.next().split("\\,");
	        
	        vendors.add(new Vendor(datas[0].replaceAll("[\r\n]+", ""), datas[1].replaceAll("[\r\n]+", ""),datas[2].replaceAll("[\r\n]+", "")));
	        
	    }
	    
	    scanner.close();
	    
	    return vendors;
		
	}
	
	public static void displayAll(List<?> listLoaded) {
		for ( Object object : listLoaded) {
			System.out.println(object.toString());
		}
	}

}
