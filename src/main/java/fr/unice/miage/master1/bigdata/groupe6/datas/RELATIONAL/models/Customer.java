package fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	
	private String id;
	private String firstName;
	private String lastName;
	private String gender;
	private String birthdate;
	private String createDate;
	private String location;
	private String browserUsed;
	private int place;
	
	private List<String> friends = new ArrayList<String>();
	
	public Customer(String id, String firstName, String lastName, String gender, String birthdate, String createDate,
			String location, String browserUsed, int place) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthdate = birthdate;
		this.createDate = createDate;
		this.location = location;
		this.browserUsed = browserUsed;
		this.place = place;
	}
	
	public List<String> getFriends(){
		return this.friends;
	}
	
	public void addFriend(String idFriend){
		this.friends.add(idFriend);
	}
	

	public String getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getGender() {
		return gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getLocation() {
		return location;
	}

	public String getBrowserUsed() {
		return browserUsed;
	}

	public int getPlace() {
		return place;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender
				+ ", birthdate=" + birthdate + ", createDate=" + createDate + ", location=" + location
				+ ", browserUsed=" + browserUsed + ", place=" + place + ", friends=" + friends + "]";
	}

}
