package fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models;

public class Vendor {
	
	private String Vendor;
	private String Country;
	private String Industry;
	
	public Vendor(String vendor, String country, String industry) {
		Vendor = vendor;
		Country = country;
		Industry = industry;
	}

	public String getVendor() {
		return Vendor;
	}

	public String getCountry() {
		return Country;
	}

	public String getIndustry() {
		return Industry;
	}
	
	public String toString() {
		return "Vendor [Vendor=" + Vendor + ", Country=" + Country + ", Industry=" + Industry + "]";
	}
	

}
