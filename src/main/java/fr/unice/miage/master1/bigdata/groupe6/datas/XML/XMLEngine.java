package fr.unice.miage.master1.bigdata.groupe6.datas.XML;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Orderline;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.RELATIONALEngine;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice;
import fr.unice.miage.master1.bigdata.groupe6.hbase.HBaseEngine;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;

public class XMLEngine {
	public static void launch() throws Exception {
		List<Invoice> invoices = new ArrayList<>();
		invoices.addAll(loadDataFromFile());
		for(Invoice invoice : invoices) {
			System.out.println(invoice);
		}
		HBaseEngine hbaseClient = new HBaseEngine();
		
		importDatasToHBase(hbaseClient);
		exportDatasFromHBase(hbaseClient);
		
	}
	
	public static void launchLessHbase() throws Exception {
		System.out.println("Start");
		// Load JSON data into list of Order objects
		System.out.println("Load data from file");
		List<Invoice> invoicesIn = loadDataFromFile();
		// Display list of Order objects
		displayAllInvoices(invoicesIn);
		System.out.println("End");
	}

	
	public static void importDatasToHBase(HBaseEngine hbaseClient) throws Exception {
		// Load JSON data into list of Order objects
				List<Invoice> invoices = loadDataFromFile();
				// Display list of Order objects 
				//displayAllOrders(orders);
				System.out.println(getOrderByOrderId(invoices, "94b65115-f856-41dd-8e38-5c8982fe7ba9").toString());
				
				// Convert list of Order object to list of Record object
				List<Record> recordsOrders = objectsToRecords(invoices);
				
				// Store records into HBase
				storeRecordsToHBase(hbaseClient,recordsOrders);
	}
	
	public static void exportDatasFromHBase(HBaseEngine hbaseClient) throws Exception {
		// Load HBase data into list of Record objects
		List<Record> recordsInvoices = loadDataFromHBase(hbaseClient);
		
		// Convert list of Record object to list of Order object
		List<Invoice> invoices = recordsToObjects(recordsInvoices);
		// Display list of Order objects 
		//displayAllOrders(orders);
		System.out.println(getOrderByOrderId(invoices, "94b65115-f856-41dd-8e38-5c8982fe7ba9").toString());
	}
	
	public static List<Invoice> loadDataFromFile() throws ParserConfigurationException, SAXException, IOException{
		
		List<Invoice> invoices = new ArrayList<>();
		
		String path = XMLEngine.class.getResource("Invoice.xml").getPath();
		
		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("Invoice.xml");
		
		int totalNodes = nList.getLength();
		
		for (int temp = 0; temp < totalNodes; temp++) {
			
			
			Node nNode = nList.item(temp);
			
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;

				String OrderId = eElement.getElementsByTagName("OrderId").item(0).getTextContent();
				String PersonId = eElement.getElementsByTagName("PersonId").item(0).getTextContent();
				String OrderDate = eElement.getElementsByTagName("OrderDate").item(0).getTextContent();
				double TotalPrice = Double.valueOf(eElement.getElementsByTagName("TotalPrice").item(0).getTextContent());
				
				NodeList Orderlines = eElement.getElementsByTagName("Orderline");
				
				List<Orderline> orderlines = new ArrayList<>();
				
				for(int temp2 = 0; temp2 < Orderlines.getLength(); temp2++) {
					
					Node orderlineNode = Orderlines.item(temp2);
					
					if (orderlineNode.getNodeType() == Node.ELEMENT_NODE) {
						
						Element eElementOrderline = (Element) orderlineNode;
						
						String productId = eElementOrderline.getElementsByTagName("productId").item(0).getTextContent();
						String asin = eElementOrderline.getElementsByTagName("asin").item(0).getTextContent();
						String title = eElementOrderline.getElementsByTagName("title").item(0).getTextContent();
						double price = Double.valueOf(eElementOrderline.getElementsByTagName("price").item(0).getTextContent());
						String brand = eElementOrderline.getElementsByTagName("brand").item(0).getTextContent();
						
						orderlines.add(new Orderline(productId, asin, title, price, brand));
						
					}
					
				}
				
				invoices.add(new Invoice(OrderId, PersonId, OrderDate, TotalPrice, orderlines));
				
			}
			
		}
		
		return invoices;
		
	}
	
	public static void displayAllInvoices(List<Invoice> invoicesLoaded) {
		for ( Invoice invoiceLoaded : invoicesLoaded) {
			System.out.println(invoiceLoaded.toString());
		}
	}
	
	public static List<Record> objectsToRecords(List<Invoice> invoices){
		
		List<Record> records = new ArrayList<Record>();
		for (Invoice invoice : invoices) {
			records.add(invoice.toRecord());
		}
		
		return records;
		
	}
	
	public static void storeRecordsToHBase(HBaseEngine hbaseClient,List<Record> records) throws Exception {
		
		System.out.println("\nInitialize Hbase client ... \n");
		
		System.out.println("\nDeclare table structure into Hbase database ... \n");
		boolean tableIsCreated = hbaseClient.createTable(Invoice.generateTableStruct());
		
		if(tableIsCreated) {
			System.out.println("Put record objects into Hbase database ...\n");
			hbaseClient.createRecords(records);
		}
		else {
			System.err.println("\n[WARN] Datas in table already exist ... ignoring datas insertions\n");
		}
		
	}
	
	public static List<Record> loadDataFromHBase(HBaseEngine hbaseClient) {
		
		return hbaseClient.getRecords("Invoice");
	
	}
	
	public static List<Invoice> recordsToObjects(List<Record> records){
			return null;
			
		}
	
	public static Invoice getOrderByOrderId(List<Invoice> invoices, String invoiceId) {
		
		Invoice invoiceReturn = null;
		
		for (Invoice invoice : invoices) {
			
			if(invoice.getOrderId().contentEquals(invoiceId)) {
				invoiceReturn = invoice;
				return invoiceReturn;
			}
		}
		
		return invoiceReturn;
	}
	
	public static List<Invoice> getInvoicesByCustomer(String idCustomer,List<Invoice> invoices) {
		
		return invoices.stream()
				  .filter(invoice -> idCustomer.contentEquals(invoice.getPersonId()))
				  .collect(Collectors.toList());
		
	}
	
	public static List<Customer> getCommonCustomers(Customer customer1, Customer customer2, List<LinkCustomerCustomer> linksCustomerCustomer, List<Customer> customers){
		
		List<LinkCustomerCustomer> linksCustomer1Customer1 = linksCustomerCustomer.stream()
		.filter(link -> customer1.getId().contentEquals(link.getCustomerId1()))
		.collect(Collectors.toList());
		
		List<LinkCustomerCustomer> linksCustomer1Customer2 = linksCustomerCustomer.stream()
		.filter(link -> customer1.getId().contentEquals(link.getCustomerId2()))
		.collect(Collectors.toList());
		
		List<LinkCustomerCustomer> linksCustomer2Customer1 = linksCustomerCustomer.stream()
		.filter(link -> customer2.getId().contentEquals(link.getCustomerId1()))
		.collect(Collectors.toList());
		
		List<LinkCustomerCustomer> linksCustomer2Customer2 = linksCustomerCustomer.stream()
		.filter(link -> customer2.getId().contentEquals(link.getCustomerId2()))
		.collect(Collectors.toList());
		
		List<String> stringsCustomer1 = new ArrayList<String>();
		
		for (LinkCustomerCustomer linkCustomer1Customer1 : linksCustomer1Customer1) {
			stringsCustomer1.add(linkCustomer1Customer1.getCustomerId2());
		}
		
		for (LinkCustomerCustomer linkCustomer1Customer2 : linksCustomer1Customer2) {
			stringsCustomer1.add(linkCustomer1Customer2.getCustomerId1());
		}
		
		List<String> stringsCustomer2 = new ArrayList<String>();
		
		for (LinkCustomerCustomer linkCustomer2Customer1 : linksCustomer2Customer1) {
			stringsCustomer2.add(linkCustomer2Customer1.getCustomerId2());
		}
		
		for (LinkCustomerCustomer linkCustomer2Customer2 : linksCustomer2Customer2) {
			stringsCustomer2.add(linkCustomer2Customer2.getCustomerId1());
		}
		
		List<String> commonCustomersId = stringsCustomer1;
		commonCustomersId.retainAll(stringsCustomer2);
		
		List<Customer> commonCustomers = new ArrayList<Customer>();
		
		for (String idCustomer : commonCustomersId) {
			commonCustomers.add(RELATIONALEngine.getCustomerById(idCustomer, customers));
		}
		
		return commonCustomers;
	}
	
}
