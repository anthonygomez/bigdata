package fr.unice.miage.master1.bigdata.groupe6.datas.XML.models;

import java.util.ArrayList;
import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Orderline;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Data;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.TableStruct;

public class Invoice {

	private String orderId;
	private String personId;
	private String orderDate;
	private double totalPrice;
	
	private List<Orderline> orderlines;
	
	public Invoice(String unOrderId, String unPersonId, String unOrderDate, Double double1, List<Orderline> desOrderLines) {
		this.orderId = unOrderId;
		this.personId = unPersonId;
		this.orderDate = unOrderDate;
		this.totalPrice = double1;
		this.orderlines = desOrderLines;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<Orderline> getOrderlines() {
		return orderlines;
	}

	public void setOrderlines(List<Orderline> orderlines) {
		this.orderlines = orderlines;
	}
	
	public String toString() {
		return 
				"OrderId : " + this.orderId + "\n" + 
				"PersonId : " + this.personId + "\n" + 
				"OrderDate : " + this.orderDate + "\n" + 
				"TotalPrice : " + this.totalPrice + "\n" +
				"OrderLines : " + this.orderlines.toString();
	}
	
	public Record toRecord() {
		ArrayList<Data> datas = new ArrayList<Data>();

		datas.add(new Data("invoice", "orderDate", this.orderDate));
		datas.add(new Data ("invoice", "totalprice", String.valueOf(this.totalPrice)));
		datas.add(new Data("invoice", "personId", this.personId));
		
		for (Orderline orderline : this.orderlines) {
			
			datas.add(new Data("orderline", "asin", orderline.getAsin()));
			datas.add(new Data("orderline", "brand", orderline.getBrand()));
			datas.add(new Data("orderline", "price", String.valueOf(orderline.getPrice())));
			datas.add(new Data("orderline", "productId", orderline.getProductId()));
			datas.add(new Data("orderline", "title", orderline.getTitle()));
			
		}
		
		return new Record("invoice", orderId, datas);
	}
	
	public static TableStruct generateTableStruct() {
		//TODO
		return null;
	}
}
