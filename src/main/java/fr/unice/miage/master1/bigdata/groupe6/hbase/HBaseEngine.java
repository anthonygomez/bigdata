package fr.unice.miage.master1.bigdata.groupe6.hbase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import com.google.protobuf.ServiceException;

import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Data;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.Record;
import fr.unice.miage.master1.bigdata.groupe6.hbase.models.TableStruct;

public class HBaseEngine {
	
	protected Admin admin = null;
	protected Configuration config;
	protected Connection connection;
	
	
	public HBaseEngine() {
		
		this.config = HBaseConfiguration.create();
   	 
    	String path = HBaseEngine.class.getResource("hbase-site.xml").getPath();
    	this.config.addResource(new Path(path));
    	
    	try {
    		HBaseAdmin.checkHBaseAvailable(this.config);
    	} catch (ServiceException | IOException e) {
    		e.printStackTrace();
    	}
    	
    	try {
    	  this.connection = ConnectionFactory.createConnection(this.config);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		try {
			this.admin = connection.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean createTable(TableStruct tableStruct) {
		
		boolean isCreated = false;
		
		 try {
			 
			 TableName tableName = TableName.valueOf(tableStruct.getName());
			 
			if (admin.tableExists(tableName) == false) {
				
				HTableDescriptor desc = new HTableDescriptor(tableName);
				
				for (String family : tableStruct.getFamilies()) {
					
					desc.addFamily(new HColumnDescriptor(family));
				}
				
				try {
						admin.createTable(desc);
						isCreated = true;
					}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				System.err.println("\n[WARN] Table name already exist ... ignoring table generation\n");
			}
		}
		 
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return isCreated;
	}
	
	public void deleteTable(String nameTable) {
		
		TableName tableName = TableName.valueOf(nameTable);
		
		try {
			admin.disableTable(tableName);
			admin.deleteTable(tableName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createRecord(Record record) throws Exception {
		
		TableName tableName = TableName.valueOf(record.getTable());
		
		byte[] key = Bytes.toBytes(record.getKey());
		
		Put put = new Put(key);
		
		for (Data data : record.getDatas()) {
			
			if(isEmpty(record.getTable(), record.getKey(), data.getFamily(), data.getQualifer())) {
				put.addImmutable(data.getFamily().getBytes(), data.getQualifer().getBytes(), data.getValue().getBytes());
			}
			else {
				throw new Exception("Datas already exist");
			}
		}
		
		try {
			Table table = this.connection.getTable(tableName);
			table.put(put);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createRecords(List<Record> records) throws Exception {
			
			for (Record record : records) {
				
				TableName tableName = TableName.valueOf(record.getTable());
				
				byte[] key = Bytes.toBytes(record.getKey());
				
				Put put = new Put(key);
				
				for (Data data : record.getDatas()) {
					
					if(isEmpty(record.getTable(), record.getKey(), data.getFamily(), data.getQualifer())) {
						
						put.addImmutable(data.getFamily().getBytes(), data.getQualifer().getBytes(), data.getValue().getBytes());
					}
					else {
						throw new Exception("Datas already exist");
					}
				}
				
				try {
					Table table = this.connection.getTable(tableName);
					table.put(put);
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	
	
		public void updateRecord(Record record) throws Exception {
				
				TableName tableName = TableName.valueOf(record.getTable());
				
				byte[] key = Bytes.toBytes(record.getKey());
				
				Put put = new Put(key);
				
				for (Data data : record.getDatas()) {
					
					if(!isEmpty(record.getTable(), record.getKey(), data.getFamily(), data.getQualifer())) {
						put.addImmutable(data.getFamily().getBytes(), data.getQualifer().getBytes(), data.getValue().getBytes());
					}
					else {
						throw new Exception("Datas not exist");
					}
				}
				
				try {
					Table table = this.connection.getTable(tableName);
					table.put(put);
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		
		public void updateRecords(List<Record> records) throws Exception {
				
				for (Record record : records) {
					
					TableName tableName = TableName.valueOf(record.getTable());
					
					byte[] key = Bytes.toBytes(record.getKey());
					
					Put put = new Put(key);
					
					for (Data data : record.getDatas()) {
						
						if(!isEmpty(record.getTable(), record.getKey(), data.getFamily(), data.getQualifer())) {
							
							put.addImmutable(data.getFamily().getBytes(), data.getQualifer().getBytes(), data.getValue().getBytes());
						}
						else {
							throw new Exception("Datas not exist");
						}
					}
					
					try {
						Table table = this.connection.getTable(tableName);
						table.put(put);
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
	
	public Boolean isEmpty(String table, String key, String family, String qualifer) {
		
		boolean isEmpty = true;
		
		try {
			this.getValue(table, key, family, qualifer);
			isEmpty = false;
		} catch (Exception e) {
			isEmpty = true;
		}
		
		return isEmpty;
	}
	
	public String getValue(String table, String key, String family, String qualifer) {
		
		TableName tableName = TableName.valueOf(table);
		
		Get get = new Get(Bytes.toBytes(key));
		
		Result result = null;
		
		try {
			
			result = this.connection.getTable(tableName).get(get);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		byte[] valueBytes = result.getValue(family.getBytes(), qualifer.getBytes());
		
		String value = null;
		
		try {
			
			value = new String(valueBytes, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;

	}
	
	public List<String> getValues(String table, List<String> keys, String family, String qualifer) {
			
			List<String> values = new ArrayList<String>();
			
			for (String key : keys) {
				
				values.add(getValue(table, key, family, qualifer));
				
			}
			
			return values;
	
		}
	
	@SuppressWarnings("deprecation")
	public List<Record> getRecords(String tableName) {
		
		List<Record> records = new ArrayList<Record>();
		
		Table table = null;
		try {
			table = connection.getTable(TableName.valueOf(tableName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scan scan = new Scan();
		
		ResultScanner scanner = null;
		try {
			scanner = table.getScanner(scan);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			for (Result result = scanner.next(); (result != null); result = scanner.next()) {
				
				String key = Bytes.toString(result.getRow());
				
				ArrayList<Data> datas = new ArrayList<Data>();
				
			    for(Cell cell : result.listCells()) {
			    		
						String family = Bytes.toString(cell.getFamily());
			    		String qualifer = Bytes.toString(cell.getQualifier());
			    		String value = Bytes.toString(cell.getValue());
			    		
			    		datas.add(new Data(family, qualifer, value));
			    		
					}
			    
			    Record record = new Record(tableName, key, datas);
			    records.add(record);
			    
			    }
			
		} catch (IOException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return records;
	}
	
	public void deleteRecord(String table, String key) {
		
		TableName tableName = TableName.valueOf(table);
		
		Delete delete = new Delete(Bytes.toBytes(key));
		
		try {
			this.connection.getTable(tableName).delete(delete);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void deleteRecord(Record record) {
			
			TableName tableName = TableName.valueOf(record.getTable());
			
			Delete delete = new Delete(Bytes.toBytes(record.getKey()));
			
			try {
				this.connection.getTable(tableName).delete(delete);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

}
