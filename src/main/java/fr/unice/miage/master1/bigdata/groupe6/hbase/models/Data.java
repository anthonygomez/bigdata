package fr.unice.miage.master1.bigdata.groupe6.hbase.models;

public class Data {

	private String family;
	private String qualifer;
	private String value;
	
	public Data(String family, String qualifer, String value) {
		this.family = family;
		this.qualifer = qualifer;
		this.value = value;
	}
	
	public String getFamily() {
		return family;
	}

	public String getQualifer() {
		return qualifer;
	}

	public String getValue() {
		return value;
	}
	
	public String toString() {
		return 
				"\n\nFamily : "+this.family+
				"\nQualifer : "+this.qualifer+
				"\nValue : "+this.value
				; 
	}

}

