package fr.unice.miage.master1.bigdata.groupe6.hbase.models;

import java.util.ArrayList;
import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Orderline;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice; 
import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models.Feedback;

public class Record {
	
	private String table;
	private String key;
	private ArrayList<Data> datas;
	
	public Record(String table, String key, ArrayList<Data> datas) {
		
		this.table = table;
		this.key = key;
		this.datas = datas;
	}

	public String getTable() {
		return table;
	}

	public String getKey() {
		return key;
	}

	public ArrayList<Data> getDatas() {
		return datas;
	}

	public String toString() {
		return 
				"\n---\nTable : "+this.table+
				"\nKey : "+this.key+
				"\nDatas : "+this.datas.toString();
	}

	// TODO
	public Invoice toInvoice() {return null;}
	
	public Order toOrder() {
		
		String OrderId = key;
		
		String OrderDate = null;
		String PersonId = null;
		String TotalPrice = null;
		
		String asin = null;
		String brand = null;
		double price = 0;
		String productId = null;
		String title = null;
		
		List<Orderline> Orderlines = new ArrayList<Orderline>();
		
		for (Data data : datas) {
			
			if(data.getFamily().contentEquals("order")) {
				if(data.getQualifer().contentEquals("date")) {
					OrderDate = data.getValue();
				}
				else if(data.getQualifer().contentEquals("totalprice")) {
					TotalPrice = data.getValue();
				}
			}
			
			else if(data.getFamily().contentEquals("person")) {
				if(data.getQualifer().contentEquals("id")) {
					PersonId = data.getValue();
				}
			}
			
			else if(data.getFamily().contentEquals("orderline")) {
				
				if(data.getQualifer().contentEquals("asin")) {
					asin = data.getValue();
				}
				else if(data.getQualifer().contentEquals("brand")) {
					brand = data.getValue();
				}
				else if(data.getQualifer().contentEquals("price")) {
					price = Double.valueOf(data.getValue());
				}
				else if(data.getQualifer().contentEquals("productId")) {
					productId = data.getValue();
				}
				else if(data.getQualifer().contentEquals("title")) {
					title = data.getValue();
				}
				
				if (asin != null && brand != null && price != 0 && productId != null && title != null ) {
					
					Orderlines.add(new Orderline(productId, asin, title, price, brand));
					
					asin = null;
					brand = null;
					price = 0;
					productId = null;
					title = null;
					
				}
				
			}
			
		}
		
		return new Order(OrderId, PersonId,OrderDate,Double.valueOf(TotalPrice),Orderlines);
		
	}
	
	public Feedback toFeedback() {
		
		String asin = key;
		
		String PersonId = null;
		String feedback = null;
		
		for (Data data : datas) {
			
			if(data.getFamily().contentEquals("infos")) {
				if(data.getQualifer().contentEquals("asin")) {
					feedback = data.getValue();
				}
			}
			
			if(data.getFamily().contentEquals("person")) {
				if(data.getQualifer().contentEquals("id")) {
					PersonId = data.getValue();
				}
			}
			
			if(data.getFamily().contentEquals("infos")) {
				if(data.getQualifer().contentEquals("feedback")) {
					asin = data.getValue();
				}
			}
		}
		
		return new Feedback(asin, PersonId,feedback);
		
	}
	
}

