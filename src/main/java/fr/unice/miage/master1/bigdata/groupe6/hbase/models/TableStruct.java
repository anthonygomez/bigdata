package fr.unice.miage.master1.bigdata.groupe6.hbase.models;

import java.util.ArrayList;
import java.util.List;

public class TableStruct {
	
	private String name;
	private List<String> families = new ArrayList<String>();
	
	public TableStruct(String name, List<String> families) {
		this.name = name;
		this.families = families;
	}
	
	public String getName() {
		return name;
	}
	public List<String> getFamilies() {
		return families;
	}

}

