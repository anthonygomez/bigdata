package fr.unice.miage.master1.bigdata.groupe6.models;

import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkCustomerTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostCustomer;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.LinkPostTag;
import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.Post;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Product;
import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models.Feedback;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Vendor;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice;

public class DataList {
	
	private List<Customer> customers;
	private List<Feedback> feedbacks;
	private List<Vendor> vendors;
	private List<Order> orders;
	private List<Product> products;
	private List<Invoice> invoices;
	private List<Post> posts;
	
	private List<LinkCustomerCustomer> linksCustomerCustomer;
	private List<LinkCustomerTag> linksCustomerTag;
	private List<LinkPostTag> linksPostTag;
	private List<LinkPostCustomer> linksPostCustomer;
	
	public DataList(List<Customer> customers, List<Feedback> feedbacks, List<Vendor> vendors, List<Order> orders,
			List<Product> products, List<Invoice> invoices, List<Post> posts,
			List<LinkCustomerCustomer> linksCustomerCustomer, List<LinkCustomerTag> linksCustomerTag,
			List<LinkPostTag> linksPostTag, List<LinkPostCustomer> linksPostCustomer) {
		this.customers = customers;
		this.feedbacks = feedbacks;
		this.vendors = vendors;
		this.orders = orders;
		this.products = products;
		this.invoices = invoices;
		this.posts = posts;
		this.linksCustomerCustomer = linksCustomerCustomer;
		this.linksCustomerTag = linksCustomerTag;
		this.linksPostTag = linksPostTag;
		this.linksPostCustomer = linksPostCustomer;
	}
	
	public List<LinkPostCustomer> getLinkPostCustomer(){
		return linksPostCustomer;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public List<Vendor> getVendors() {
		return vendors;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public List<Product> getProducts() {
		return products;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public List<LinkCustomerCustomer> getLinksCustomerCustomer() {
		return linksCustomerCustomer;
	}

	public List<LinkCustomerTag> getLinksCustomerTag() {
		return linksCustomerTag;
	}

	public List<LinkPostTag> getLinksPostTag() {
		return linksPostTag;
	}
	

}
