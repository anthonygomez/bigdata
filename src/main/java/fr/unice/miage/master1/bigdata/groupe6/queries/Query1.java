package fr.unice.miage.master1.bigdata.groupe6.queries;

import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.GRAPH.models.Post;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Order;
import fr.unice.miage.master1.bigdata.groupe6.datas.JSON.models.Product;
import fr.unice.miage.master1.bigdata.groupe6.datas.KEYVALUE.models.Feedback;
import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;
import fr.unice.miage.master1.bigdata.groupe6.datas.XML.models.Invoice;

public class Query1 {
	
	private Customer customer;
	private List<Order> orders;
	private List<Invoice> invoices;
	private List<Feedback> feedbacks;
	private List <Post> posts;
	private String tagMostUsed;
	private Product productMostOrdered;
	
	public Query1(Customer customer, List<Order> orders, List<Invoice> invoices, List<Feedback> feedbacks,
			List<Post> posts, String tagMostUsed, Product productMostOrdered) {
		this.customer = customer;
		this.orders = orders;
		this.invoices = invoices;
		this.feedbacks = feedbacks;
		this.posts = posts;
		this.tagMostUsed = tagMostUsed;
		this.productMostOrdered = productMostOrdered;
	}

	public Customer getCustomer() {
		return customer;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public String getTagMostUsed() {
		return tagMostUsed;
	}

	public Product getProductMostOrdered() {
		return productMostOrdered;
	}

	@Override
	public String toString() {
		return "Query1 [customer=" + customer + ", orders=" + orders + ", invoices=" + invoices + ", feedbacks="
				+ feedbacks + ", posts=" + posts + ", tagMostUsed=" + tagMostUsed + ", productMostOrdered="
				+ productMostOrdered + "]";
	}

}
