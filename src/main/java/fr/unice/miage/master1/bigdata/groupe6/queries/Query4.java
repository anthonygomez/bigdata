package fr.unice.miage.master1.bigdata.groupe6.queries;

import java.util.List;

import fr.unice.miage.master1.bigdata.groupe6.datas.RELATIONAL.models.Customer;

public class Query4 {
	
	private Customer firstCustomerSpendMore;
	private Customer secondCustomerSpendMore;
	
	private List<Customer> communFriends;

	public Query4(Customer firstCustomerSpendMore, Customer secondCustomerSpendMore, List<Customer> communFriends) {
		this.firstCustomerSpendMore = firstCustomerSpendMore;
		this.secondCustomerSpendMore = secondCustomerSpendMore;
		this.communFriends = communFriends;
	}

	public Customer getFirstCustomerSpendMore() {
		return firstCustomerSpendMore;
	}

	public Customer getSecondCustomerSpendMore() {
		return secondCustomerSpendMore;
	}

	public List<Customer> getCommunFriends() {
		return communFriends;
	}

	@Override
	public String toString() {
		return "Query4 [firstCustomerSpendMore=" + firstCustomerSpendMore + ", secondCustomerSpendMore="
				+ secondCustomerSpendMore + ", communFriends=" + communFriends + "]";
	}

}
